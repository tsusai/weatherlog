package com.kangjusang.weatherlog.presenter;

import android.util.Log;

import com.kangjusang.weatherlog.MVPWeather;
import com.kangjusang.weatherlog.data.MetaWeather;
import com.kangjusang.weatherlog.network.MetaWeatherNetwork;
import com.kangjusang.weatherlog.network.WeatherNetworkInterface;

import java.util.List;

public class WeatherPresenter implements MVPWeather.Presenter {

    MVPWeather.View mView;

    public WeatherPresenter(MVPWeather.View view) {
        this.mView = view;
    }

    @Override
    public void refresh(List<String> woeids) {
        MetaWeatherNetwork metaWeather = new MetaWeatherNetwork();

        for (String woeid : woeids) {
            metaWeather.getWeather(woeid, new WeatherNetworkInterface.WeatherListener() {

                @Override
                public void onSuccess(MetaWeather weather) {
//                    Log.d("WeatherPresenter", weather.toString());
                    mView.updateResults(weather.toWeather());
                }

                @Override
                public void onFailure(Throwable throwable) {
//                    Log.d("WeatherPresenter", throwable.toString());
                }
            });
        }
    }
}
