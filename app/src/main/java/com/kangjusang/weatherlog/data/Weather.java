package com.kangjusang.weatherlog.data;

import androidx.annotation.NonNull;

public class Weather {

    public String applicable_date;
    public String weather_state_name;
    public String weather_state_icon_url;
    public String min_temp;
    public String max_temp;

    public Weather(String date, String state_name, String state_url, String min, String max) {
        this.applicable_date = date;
        this.weather_state_name = state_name;
        this.weather_state_icon_url = state_url;
        this.min_temp = min;
        this.max_temp = max;
    }

    @NonNull
    @Override
    public String toString()
    {
        return "ClassPojo [applicable_date = "+applicable_date+", weather_state_name = "+ weather_state_name +", weather_state_icon_url = "+ weather_state_icon_url +", min_temp = "+min_temp+", max_temp = "+max_temp+"]";
    }

    interface Generalizer {
        WeathersOfCity toWeather();
    }


}
