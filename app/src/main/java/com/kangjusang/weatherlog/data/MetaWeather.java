package com.kangjusang.weatherlog.data;

import java.util.ArrayList;

public class MetaWeather implements Weather.Generalizer
{
    private String sun_set;
    private Parent parent;
    private Sources[] sources;
    private String latt_long;
    private String timezone;
    private String timezone_name;
    private String woeid;
    private String sun_rise;
    private Consolidated_weather[] consolidated_weather;
    private String time;
    private String title;
    private String location_type;
    public String getSun_set ()
    {
        return sun_set;
    }
    public void setSun_set (String sun_set)
    {
        this.sun_set = sun_set;
    }
    public Parent getParent ()
    {
        return parent;
    }
    public void setParent (Parent parent)
    {
        this.parent = parent;
    }
    public Sources[] getSources ()
    {
        return sources;
    }
    public void setSources (Sources[] sources)
    {
        this.sources = sources;
    }
    public String getLatt_long ()
    {
        return latt_long;
    }
    public void setLatt_long (String latt_long)
    {
        this.latt_long = latt_long;
    }
    public String getTimezone ()
    {
        return timezone;
    }
    public void setTimezone (String timezone)
    {
        this.timezone = timezone;
    }
    public String getTimezone_name ()
    {
        return timezone_name;
    }
    public void setTimezone_name (String timezone_name)
    {
        this.timezone_name = timezone_name;
    }
    public String getWoeid ()
    {
        return woeid;
    }
    public void setWoeid (String woeid)
    {
        this.woeid = woeid;
    }
    public String getSun_rise ()
    {
        return sun_rise;
    }
    public void setSun_rise (String sun_rise)
    {
        this.sun_rise = sun_rise;
    }
    public Consolidated_weather[] getConsolidated_weather ()
    {
        return consolidated_weather;
    }
    public void setConsolidated_weather (Consolidated_weather[] consolidated_weather)
    {
        this.consolidated_weather = consolidated_weather;
    }
    public String getTime ()
    {
        return time;
    }
    public void setTime (String time)
    {
        this.time = time;
    }
    public String getTitle ()
    {
        return title;
    }
    public void setTitle (String title)
    {
        this.title = title;
    }
    public String getLocation_type ()
    {
        return location_type;
    }
    public void setLocation_type (String location_type)
    {
        this.location_type = location_type;
    }
    @Override
    public String toString()
    {
        return "ClassPojo [sun_set = "+sun_set+", parent = "+parent+", sources = "+sources+", latt_long = "+latt_long+", timezone = "+timezone+", timezone_name = "+timezone_name+", woeid = "+woeid+", sun_rise = "+sun_rise+", consolidated_weather = "+consolidated_weather+", time = "+time+", title = "+title+", location_type = "+location_type+"]";
    }

    @Override
    public WeathersOfCity toWeather() {

        ArrayList<Weather> weathers = new ArrayList<>();
        for (Consolidated_weather consolidated_weather : getConsolidated_weather()) {
            weathers.add(new Weather(consolidated_weather.applicable_date,
                                        consolidated_weather.weather_state_name,
                                        "https://metaweather.com/static/img/weather/png/64/" + consolidated_weather.weather_state_abbr + ".png",
                                        consolidated_weather.min_temp,
                                        consolidated_weather.max_temp));
        }
        WeathersOfCity weathersOfCity = new WeathersOfCity(getWoeid(), getTitle(), weathers);
        return weathersOfCity;
    }

    public class Sources
    {
        private String crawl_rate;
        private String title;
        private String slug;
        private String url;
        public String getCrawl_rate ()
        {
            return crawl_rate;
        }
        public void setCrawl_rate (String crawl_rate)
        {
            this.crawl_rate = crawl_rate;
        }
        public String getTitle ()
        {
            return title;
        }
        public void setTitle (String title)
        {
            this.title = title;
        }
        public String getSlug ()
        {
            return slug;
        }
        public void setSlug (String slug)
        {
            this.slug = slug;
        }
        public String getUrl ()
        {
            return url;
        }
        public void setUrl (String url)
        {
            this.url = url;
        }
        @Override
        public String toString()
        {
            return "ClassPojo [crawl_rate = "+crawl_rate+", title = "+title+", slug = "+slug+", url = "+url+"]";
        }
    }

    public class Parent
    {
        private String latt_long;
        private String woeid;
        private String title;
        private String location_type;
        public String getLatt_long ()
        {
            return latt_long;
        }
        public void setLatt_long (String latt_long)
        {
            this.latt_long = latt_long;
        }
        public String getWoeid ()
        {
            return woeid;
        }
        public void setWoeid (String woeid)
        {
            this.woeid = woeid;
        }
        public String getTitle ()
        {
            return title;
        }
        public void setTitle (String title)
        {
            this.title = title;
        }
        public String getLocation_type ()
        {
            return location_type;
        }
        public void setLocation_type (String location_type)
        {
            this.location_type = location_type;
        }
        @Override
        public String toString()
        {
            return "ClassPojo [latt_long = "+latt_long+", woeid = "+woeid+", title = "+title+", location_type = "+location_type+"]";
        }
    }

    public class Consolidated_weather
    {
        private String visibility;
        private String created;
        private String applicable_date;
        private String wind_direction;
        private String predictability;
        private String wind_direction_compass;
        private String weather_state_name;
        private String min_temp;
        private String weather_state_abbr;
        private String the_temp;
        private String humidity;
        private String wind_speed;
        private String id;
        private String max_temp;
        private String air_pressure;
        public String getVisibility ()
        {
            return visibility;
        }
        public void setVisibility (String visibility)
        {
            this.visibility = visibility;
        }
        public String getCreated ()
        {
            return created;
        }
        public void setCreated (String created)
        {
            this.created = created;
        }
        public String getApplicable_date ()
        {
            return applicable_date;
        }
        public void setApplicable_date (String applicable_date)
        {
            this.applicable_date = applicable_date;
        }
        public String getWind_direction ()
        {
            return wind_direction;
        }
        public void setWind_direction (String wind_direction)
        {
            this.wind_direction = wind_direction;
        }
        public String getPredictability ()
        {
            return predictability;
        }
        public void setPredictability (String predictability)
        {
            this.predictability = predictability;
        }
        public String getWind_direction_compass ()
        {
            return wind_direction_compass;
        }
        public void setWind_direction_compass (String wind_direction_compass)
        {
            this.wind_direction_compass = wind_direction_compass;
        }
        public String getWeather_state_name ()
        {
            return weather_state_name;
        }
        public void setWeather_state_name (String weather_state_name)
        {
            this.weather_state_name = weather_state_name;
        }
        public String getMin_temp ()
        {
            return min_temp;
        }
        public void setMin_temp (String min_temp)
        {
            this.min_temp = min_temp;
        }
        public String getWeather_state_abbr ()
        {
            return weather_state_abbr;
        }
        public void setWeather_state_abbr (String weather_state_abbr)
        {
            this.weather_state_abbr = weather_state_abbr;
        }
        public String getThe_temp ()
        {
            return the_temp;
        }
        public void setThe_temp (String the_temp)
        {
            this.the_temp = the_temp;
        }
        public String getHumidity ()
        {
            return humidity;
        }
        public void setHumidity (String humidity)
        {
            this.humidity = humidity;
        }
        public String getWind_speed ()
        {
            return wind_speed;
        }
        public void setWind_speed (String wind_speed)
        {
            this.wind_speed = wind_speed;
        }
        public String getId ()
        {
            return id;
        }
        public void setId (String id)
        {
            this.id = id;
        }
        public String getMax_temp ()
        {
            return max_temp;
        }
        public void setMax_temp (String max_temp)
        {
            this.max_temp = max_temp;
        }
        public String getAir_pressure ()
        {
            return air_pressure;
        }
        public void setAir_pressure (String air_pressure)
        {
            this.air_pressure = air_pressure;
        }
        @Override
        public String toString()
        {
            return "ClassPojo [visibility = "+visibility+", created = "+created+", applicable_date = "+applicable_date+", wind_direction = "+wind_direction+", predictability = "+predictability+", wind_direction_compass = "+wind_direction_compass+", weather_state_name = "+weather_state_name+", min_temp = "+min_temp+", weather_state_name = "+weather_state_abbr+", the_temp = "+the_temp+", humidity = "+humidity+", wind_speed = "+wind_speed+", id = "+id+", max_temp = "+max_temp+", air_pressure = "+air_pressure+"]";
        }
    }
}


