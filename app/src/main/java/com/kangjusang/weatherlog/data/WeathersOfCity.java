package com.kangjusang.weatherlog.data;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kangjusang.weatherlog.R;

import java.util.ArrayList;
import java.util.List;

public class WeathersOfCity {

    public String mWoeid;
    public String mCity;
    public List<Weather> mWeathers;

    public WeathersOfCity() {
        mWoeid = "Loading...";
        mCity = "Loading...";
        mWeathers = new ArrayList<Weather>();
    }

    public WeathersOfCity(String woeid, String city, List<Weather> weathers) {
        mWoeid = woeid;
        mCity = city;
        mWeathers = weathers;
    }
}


