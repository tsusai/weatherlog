package com.kangjusang.weatherlog.ui;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kangjusang.weatherlog.R;
import com.kangjusang.weatherlog.data.Weather;

import java.util.List;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder> {

    private List<Weather> mWeathers;

    public WeatherAdapter(List<Weather> weathers) {
        mWeathers = weathers;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View weatherView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_weather, parent, false);
        ViewHolder viewHolder = new ViewHolder(weatherView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
//        Log.d("WeatherAdapter", "onBindViewHolder : " + position);
        Weather weather = mWeathers.get(position);
        holder.applicable_date.setText(weather.applicable_date);
        holder.state_name.setText(weather.weather_state_name);
        Glide.with(holder.itemView.getContext()).load(weather.weather_state_icon_url).into(holder.weather_state_icon);
        holder.min_temp.setText("Min : " + Math.floor(Double.valueOf(weather.min_temp)) + "\u2103");
        holder.max_temp.setText("Max : " + Math.floor(Double.valueOf(weather.max_temp)) + "\u2103");
    }

    @Override
    public int getItemCount() {
        return mWeathers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView applicable_date;
        public TextView state_name;
        public ImageView weather_state_icon;
        public TextView min_temp;
        public TextView max_temp;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            applicable_date = itemView.findViewById(R.id.tvDate);
            state_name = itemView.findViewById(R.id.tvStateName);
            weather_state_icon = itemView.findViewById(R.id.imageView);
            min_temp = itemView.findViewById(R.id.tvMin);
            max_temp = itemView.findViewById(R.id.tvMax);
        }
    }
}
