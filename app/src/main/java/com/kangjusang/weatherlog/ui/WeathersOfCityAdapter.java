package com.kangjusang.weatherlog.ui;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.kangjusang.weatherlog.R;
import com.kangjusang.weatherlog.data.WeathersOfCity;

import java.util.List;

public class WeathersOfCityAdapter extends RecyclerView.Adapter<WeathersOfCityAdapter.ViewHolder> {

    private List<WeathersOfCity> mWeathersOfCities;

    public WeathersOfCityAdapter(List<WeathersOfCity> weathers) {
        mWeathersOfCities = weathers;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View weatherOfCityView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_weather_city, parent, false);
        ViewHolder viewHolder = new ViewHolder(weatherOfCityView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

//        Log.d("WeatherOfCityAdapter", "onBindViewHolder : " + position);

        WeathersOfCity weathersOfCity = mWeathersOfCities.get(position);
        holder.city.setText(weathersOfCity.mCity);
        WeatherAdapter weatherAdapter = new WeatherAdapter(weathersOfCity.mWeathers);
        holder.weathers.setAdapter(weatherAdapter);
    }

    @Override
    public int getItemCount() {
        return mWeathersOfCities.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView city;
        public RecyclerView weathers;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            city = itemView.findViewById(R.id.tvCity);
            weathers = itemView.findViewById(R.id.rvWeathers);
            weathers.setLayoutManager(new LinearLayoutManager(itemView.getContext()));
        }
    }
}
