package com.kangjusang.weatherlog.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;

import com.kangjusang.weatherlog.MVPWeather;
import com.kangjusang.weatherlog.R;
import com.kangjusang.weatherlog.data.Weather;
import com.kangjusang.weatherlog.data.WeathersOfCity;
import com.kangjusang.weatherlog.presenter.WeatherPresenter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements MVPWeather.View {

    ArrayList<WeathersOfCity> mWeathersOfCities = new ArrayList<>();
    ArrayList<String> mWoeids = new ArrayList<>(Arrays.asList("1132599", "44418", "2379574"));
    WeathersOfCityAdapter mWeathersOfCitiesAdapter;
    WeatherPresenter mWeatherPresenter;
    ArrayList<Boolean> mRefreshDone = new ArrayList<>();
    SwipeRefreshLayout mSwipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUi();

        mWeatherPresenter = new WeatherPresenter(this);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    mWeatherPresenter.refresh(mWoeids);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        });
        thread.start();
    }

    private void initUi() {

        for (int i = 0 ; i < mWoeids.size() ; i++) {
            mWeathersOfCities.add(new WeathersOfCity());
            mRefreshDone.add(new Boolean(false));
        }

        RecyclerView recyclerView = findViewById(R.id.rvCities);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mWeathersOfCitiesAdapter = new WeathersOfCityAdapter(mWeathersOfCities);
        recyclerView.setAdapter(mWeathersOfCitiesAdapter);

        mSwipeRefreshLayout = findViewById(R.id.swipe_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                for (int i = 0 ; i < mWoeids.size() ; i++) {
                    mRefreshDone.set(i,false);
                }
                mWeatherPresenter.refresh(mWoeids);
            }
        });

    }

    @Override
    public void updateResults(WeathersOfCity weathersOfCity) {

        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat targetFormat = new SimpleDateFormat("EEE d MMM", Locale.US);

        Date now = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        cal.set(Calendar.HOUR_OF_DAY, cal.getActualMinimum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE,      cal.getActualMinimum(Calendar.MINUTE));
        cal.set(Calendar.SECOND,      cal.getActualMinimum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getActualMinimum(Calendar.MILLISECOND));

        Date today = cal.getTime();
        cal.add(Calendar.DATE, 1);
        Date tomorrow = cal.getTime();

        for (Weather weather : weathersOfCity.mWeathers) {
            try {
                Date date = originalFormat.parse(weather.applicable_date);
                if (date.compareTo(today) == 0) {
                    weather.applicable_date = "Today";
                } else if (date.compareTo(tomorrow) == 0) {
                    weather.applicable_date = "Tomorrow";
                } else {
                    weather.applicable_date = targetFormat.format(date);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        int index = mWoeids.indexOf(weathersOfCity.mWoeid);
        mWeathersOfCities.set(index, weathersOfCity);
        mWeathersOfCitiesAdapter.notifyDataSetChanged();
        mRefreshDone.set(index, true);
        if (mRefreshDone.contains(false) == false) {
            mSwipeRefreshLayout.setRefreshing(false);
        }

    }
}
