package com.kangjusang.weatherlog.network;

import com.kangjusang.weatherlog.data.MetaWeather;

public interface WeatherNetworkInterface {

    void getWeather(String woeid, WeatherListener listener);

    interface WeatherListener {
        void onSuccess(MetaWeather weather);
        void onFailure(Throwable throwable);
    }
}
