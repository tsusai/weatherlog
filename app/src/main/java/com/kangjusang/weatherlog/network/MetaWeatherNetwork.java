package com.kangjusang.weatherlog.network;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.kangjusang.weatherlog.data.MetaWeather;

import java.io.InputStream;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public class MetaWeatherNetwork implements WeatherNetworkInterface {

    final static String PATH = "/api/location/%s/%s";

    WeatherService weatherService;

    public MetaWeatherNetwork() {
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(new Uri.Builder().scheme("https").authority("www.metaweather.com").build().toString())
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        weatherService = retrofit.create(WeatherService.class);
    }
    @Override
    public void getWeather(String woeid, final WeatherListener listener) {
        weatherService.getWeather(woeid).enqueue(new Callback<MetaWeather>() {
            @Override
            public void onResponse(Call<MetaWeather> call, Response<MetaWeather> response) {
                listener.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<MetaWeather> call, Throwable t) {
                listener.onFailure(t);
            }
        });
    }

    public void getIcon(String abbr) {
        weatherService.getIcon(abbr).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                InputStream is = response.body().byteStream();
                Bitmap bitmap = BitmapFactory.decodeStream(is);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private interface WeatherService {
        @GET("api/location/{woeid}/")
        Call<MetaWeather> getWeather(@Path("woeid") String woeid);
        @GET("static/img/weather/{abbr}.png")
        Call<ResponseBody> getIcon(@Path("abbr") String abbr);
    }
}
