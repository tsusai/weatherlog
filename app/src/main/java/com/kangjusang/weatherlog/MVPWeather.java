package com.kangjusang.weatherlog;

import com.kangjusang.weatherlog.data.Weather;
import com.kangjusang.weatherlog.data.WeathersOfCity;

import java.util.ArrayList;
import java.util.List;

public interface MVPWeather {

    interface View {
        void updateResults(WeathersOfCity weathersOfCity);
    }

    interface Presenter {
        void refresh(List<String> woeids);
    }
}
